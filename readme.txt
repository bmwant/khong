User interface with PyQt5 and Python 3 for awesome Fabric to make
deploying process easier. You can specify multiple remote servers
and deploy to all them at once. Also you can select what function
in your deploying script you want to execute on remote.

Works only when you already have Python 2.7 and Fabric installed. 
You should install Fabric in Python 2.7, but in programm use Python 3. 

Additional dependency is PyYAML for storing app's cofiguration.

# -*- coding: utf-8 -*-
__author__ = 'Most Wanted'
import os
import asyncio


@asyncio.coroutine
def subtask():
    proc = yield from asyncio.create_subprocess_exec(r'C:\Python27\python.exe',
                                                     r'D:\code\khong\python_to_run.py',
                                                     stdout=asyncio.subprocess.PIPE)
    output = yield from proc.stdout.read()
    yield from proc.wait()
    return output.decode()


def main():
    if os.name == 'nt':
        loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(loop)
    else:
        loop = asyncio.get_event_loop()
    p = loop.run_until_complete(subtask())
    #p = yield from asyncio.create_subprocess_exec(['echo', 'Hello World!'])
    return p

if __name__ == '__main__':
    main()

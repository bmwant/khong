# -*- coding: utf-8 -*-
__author__ = 'Most Wanted'
import sys
import logging
import traceback

import main

from PyQt5 import QtWidgets


def exception_hook(exctype, value, trb):
    traceback.print_exception(exctype, value, trb)
    sys.exit(1)

sys.excepthook = exception_hook


def setup_logging():
    log = logging.getLogger(__name__)
    log.setLevel(logging.DEBUG)
    log.propagate = False
    formatter = logging.Formatter('%(asctime)s :: line %(lineno)d, %(module)s [%(levelname)s] %(message)s')
    formatter.datefmt = '%H:%M:%S %d/%m/%y'
    handler = logging.StreamHandler()
    file_handler = logging.FileHandler('app.log')
    file_handler.setFormatter(formatter)
    handler.setFormatter(formatter)
    log.addHandler(file_handler)
    return log


logger = setup_logging()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    form = main.KhongApp()
    form.show()
    sys.exit(app.exec())


# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'views\add_server.ui'
#
# Created by: PyQt5 UI code generator 5.5
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(300, 237)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(120, 200, 161, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.host_edit = QtWidgets.QLineEdit(Dialog)
        self.host_edit.setGeometry(QtCore.QRect(20, 80, 261, 20))
        self.host_edit.setObjectName("host_edit")
        self.user_name_edit = QtWidgets.QLineEdit(Dialog)
        self.user_name_edit.setGeometry(QtCore.QRect(20, 130, 91, 20))
        self.user_name_edit.setObjectName("user_name_edit")
        self.user_pass_edit = QtWidgets.QLineEdit(Dialog)
        self.user_pass_edit.setGeometry(QtCore.QRect(122, 130, 161, 20))
        self.user_pass_edit.setObjectName("user_pass_edit")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(20, 60, 47, 13))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(20, 110, 47, 13))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(124, 110, 61, 16))
        self.label_3.setObjectName("label_3")
        self.name_edit = QtWidgets.QLineEdit(Dialog)
        self.name_edit.setGeometry(QtCore.QRect(20, 30, 261, 20))
        self.name_edit.setObjectName("name_edit")
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(20, 10, 47, 13))
        self.label_4.setObjectName("label_4")
        self.test_conn_btn = QtWidgets.QPushButton(Dialog)
        self.test_conn_btn.setGeometry(QtCore.QRect(27, 166, 251, 23))
        self.test_conn_btn.setObjectName("test_conn_btn")

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        Dialog.setTabOrder(self.name_edit, self.host_edit)
        Dialog.setTabOrder(self.host_edit, self.user_name_edit)
        Dialog.setTabOrder(self.user_name_edit, self.user_pass_edit)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Add new server"))
        self.label.setText(_translate("Dialog", "Host:"))
        self.label_2.setText(_translate("Dialog", "User:"))
        self.label_3.setText(_translate("Dialog", "Password:"))
        self.label_4.setText(_translate("Dialog", "Name:"))
        self.test_conn_btn.setText(_translate("Dialog", "Test connection"))


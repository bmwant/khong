import os
import sys
import inspect
import importlib
import subprocess

import yaml

from PyQt5 import QtGui, QtCore, QtWidgets

from app import logger
from ui import design
from ui import add_dialog
from helpers import show_info_dialog
from helpers import RemoteServer, AppConfig
from fabric_subprocess import Worker


class KhongApp(QtWidgets.QMainWindow, design.Ui_MainWindow):
    CONFIG_FILE = 'config.yml'
    DEFAULT_DEPLOYER = 'deployer.py'

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.setFixedSize(self.width(), self.height())
        self.conf = self.read_app_config()
        raise SystemError('I want system error')
        if not os.path.exists(self.conf.deployer_path):
            show_info_dialog('No deployer module\nNo such file %s' % self.conf.deployer_path)
            logger.debug('Cannot start without deployer script at %s' % self.conf.deployer_path)
            self.select_deployer_file()
            self.save_app_config()
        else:
            self.init_deployer_state()

        self.workers = []
        self.CLEAR_BRUSH = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        self.ERROR_BRUSH = QtGui.QBrush(QtGui.QColor(255, 102, 102))
        self.ERROR_BRUSH.setStyle(QtCore.Qt.SolidPattern)
        self.CLEAR_BRUSH.setStyle(QtCore.Qt.SolidPattern)

        self.commands_box.currentIndexChanged.connect(self.select_action)
        self.deploy_btn.clicked.connect(self.do_action)
        self.add_server_btn.clicked.connect(self.show_add_dialog)
        self.remove_server_btn.clicked.connect(self.remove_server)
        self.show_log_btn.clicked.connect(self.open_log_file)

        self.servers_list.doubleClicked.connect(self.edit_server)
        self.actionExit.triggered.connect(QtWidgets.qApp.quit)
        self.actionSelect_deployer_file.triggered.connect(self.select_deployer_file)
        self.actionEdit_commands.triggered.connect(self.open_deployer_explorer)

    def init_deployer_state(self):
        self.load_deployer()
        self.find_actions()
        self.select_action(0)
        self.setWindowTitle('Khong for: %s' % self.conf.deployer_path)

    def worker_result(self):
        clear_workers = True
        some_failed = False

        for worker in self.workers:
            if worker.failed:
                some_failed = True
                item = self.servers_list.item(worker.index)
                item.setBackground(self.ERROR_BRUSH)
            else:
                if worker.result is not None:
                    if not worker.processed:
                        self.output_window.append(worker.result)
                        worker.processed = True
                else:
                    clear_workers = False

        if clear_workers:  # All workers finished their jobs
            if some_failed:
                text = 'Deploying finished but there are errors for some servers\n' \
                       'See the application log file for additional information'
            else:
                text = 'Successfully deployed to all selected servers\nCongradulations!'
            show_info_dialog(text)
            self.workers = []

    def read_app_config(self):
        config_data = None
        if os.path.exists(self.CONFIG_FILE):
            with open(self.CONFIG_FILE) as fin:
                config_data = yaml.load(fin.read())

        if config_data is None:
            config_data = {}

        deployer_file = config_data['deployer'] if 'deployer' in config_data else self.DEFAULT_DEPLOYER
        servers_data = config_data['servers'] if 'servers' in config_data else {}
        servers = self.load_servers(servers_data)

        app_config = AppConfig(deployer_file, servers)
        return app_config

    def save_app_config(self):
        docs = {}
        for doc in self.conf.servers:
            # only unique servers are allowed based on md5-sum
            docs.update(doc.serialize())

        with open(self.CONFIG_FILE, 'w') as fout:
            fout.write(yaml.dump({'deployer': self.conf.deployer_path}, default_flow_style=False))
            fout.write(yaml.dump({'servers': docs}, default_flow_style=False))

    def do_action(self):
        action_name = self.commands_box.currentText()
        for item_index in range(self.servers_list.count()):
            item = self.servers_list.item(item_index)
            item.setBackground(self.CLEAR_BRUSH)
            server = self.conf.servers[item_index]
            if item.checkState() == 2:
                fab_conf = {
                    'deployer': self.conf.deployer_path,
                    'cdir': self.conf.deployer_dir,
                    'command': action_name,
                    'host': server.host,
                    'user': server.user_name,
                    'password': server.user_pass
                }
                new_worker = Worker(fab_conf=fab_conf, index=item_index)
                new_worker.finished.connect(self.worker_result)
                self.workers.append(new_worker)
                new_worker.start()
        self.output_window.append('Started deploying for %s remote servers\n' % len(self.workers))

    def remove_server(self):
        update = False
        for item_index in reversed(range(self.servers_list.count())):
            item = self.servers_list.item(item_index)
            if item.checkState() == 2:
                index_remove = self.servers_list.indexFromItem(item).row()
                self.servers_list.takeItem(index_remove)
                self.conf.servers.pop(index_remove)
                update = True
        if update:
            self.save_app_config()

    def edit_server(self, item_index):
        index = item_index.row()
        server_item = self.conf.servers[index]
        u = add_dialog.Ui_Dialog()
        dialog = QtWidgets.QDialog()
        u.setupUi(dialog)
        dialog.setFixedSize(dialog.size())
        u.host_edit.setText(server_item.host)
        u.name_edit.setText(server_item.name)
        u.user_name_edit.setText(server_item.user_name)
        u.user_pass_edit.setText(server_item.user_pass)

        dialog.show()
        result = dialog.exec_()
        if result:
            server_item.name = u.name_edit.text()
            server_item.host = u.host_edit.text()
            server_item.user_name = u.user_name_edit.text()
            server_item.user_pass = u.user_pass_edit.text()
            list_item = self.servers_list.item(index)
            list_item.setText(str(server_item))

            self.save_app_config()

    def load_servers(self, servers_data):
        servers = []
        for key, value in servers_data.items():
            loaded_server = RemoteServer(name=value['name'],
                                         host=value['host'],
                                         user_name=value['user'],
                                         user_pass=value['pass'])
            servers.append(loaded_server)

        for server in servers:
            self.add_server_to_list_view(server)

        return servers

    def add_server_to_list_view(self, server):
        new_item = QtWidgets.QListWidgetItem()
        new_item.setText(str(server))
        new_item.setCheckState(0)  # todo: I don't know where all these constants are located
        self.servers_list.addItem(new_item)

    def select_action(self, index):
        # !Also called when current index changes so this is the workaround
        action_name = self.commands_box.currentText()
        action = getattr(self.deployer, action_name, None)
        if action is not None:
            if action.__doc__ is not None:
                description = action.__doc__.strip()
            else:
                description = 'No description found for this function.\nMaybe this is not deploying function'
            self.action_description.setText(description)
        else:
            logger.error('Current index changed because of clean all items')

    def find_actions(self):
        self.commands_box.clear()  # how to enable debug mode
        actions = inspect.getmembers(self.deployer, inspect.isfunction)
        for action in actions:
            self.commands_box.addItem(action[0])

    def show_add_dialog(self):
        u = add_dialog.Ui_Dialog()
        dialog = QtWidgets.QDialog()
        u.setupUi(dialog)
        dialog.setFixedSize(dialog.size())
        dialog.show()
        result = dialog.exec()

        if result:
            name = u.name_edit.text()
            host = u.host_edit.text()
            user_name = u.user_name_edit.text()
            user_pass = u.user_pass_edit.text()
            new_server = RemoteServer(name=name,
                                      host=host,
                                      user_name=user_name,
                                      user_pass=user_pass)
            self.conf.servers.append(new_server)
            self.add_server_to_list_view(new_server)
            self.save_app_config()

    def select_deployer_file(self):
        """
        Show select file dialog to allow user custom select of deployer file
        """
        dialog = QtWidgets.QFileDialog()
        file_path = dialog.getOpenFileName()[0]
        if not file_path:
            return

        self.conf.deployer_path = file_path

        self.init_deployer_state()

    def load_deployer(self):
        """
        Append deployer's directory to sys.path to make module importable
        and then import it by module name
        """
        sys.path.append(self.conf.deployer_dir)
        self.deployer = importlib.import_module(self.conf.deployer_module)

    def open_deployer_explorer(self):
        logger.info('Trying to select deployer file %s' % self.conf.deployer_path)
        select_what = os.path.abspath(self.conf.deployer_path)  # Because windows does not like forward slashes
        subprocess.call(['explorer', '/select,%s' % select_what])

    def open_log_file(self):
        os.startfile('app.log')
